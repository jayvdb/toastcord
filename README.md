# ToastCord

Collection of useful utilities for discord.py (and maybe Hikari :P)


### Credits

A massive thanks to Espy (Nekoka.tt) for the amazing CI scripts and docs generation. All credit goes to him, as I've just did some tweaking. The files in question come from Hikari's [main](https://gitlab.com/nekokatt/hikari) and [core](https://gitlab.com/nekokatt/hikari.core) repositories.

