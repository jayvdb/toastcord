Toastcord documentation
#######################

This is for version |version| -- on PyPi, this is referred to as |release|

Toastcord is licensed under the GNU LGPLv3 https://www.gnu.org/licenses/lgpl-3.0.en.html

Packages and submodules
-----------------------

.. autosummary::
    :toctree: .

    toastcord
    

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`