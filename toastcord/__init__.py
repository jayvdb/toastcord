#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright © Tmpod 2019
#
# This file is part of ToastCord.
#
# ToastCord is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ToastCord is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with ToastCord. If not, see <https://www.gnu.org/licenses/>.
"""
Collection of useful utilities for discord.py (and maybe Hikari :P)
"""

__author__ = "Tmpod"
__contributors__ = set()
__copyright__ = f"© 2019 {__author__}"
__license__ = "LGPLv3"

__version__ = "0.0.2"
__url__ = f"https://gitlab.com/{__author__}/toastcord"
